import json
from datetime import datetime, timedelta, timezone
from http import HTTPStatus

import pytest

pytestmark = pytest.mark.asyncio


async def test_positions_returns_200(client, bbox):
    response = await client.get('/positions/?{}'.format(bbox))
    assert response.status == HTTPStatus.OK
    assert response.body == '{"data":[]}'


async def test_positions_returns_json(client, bbox):
    response = await client.get('/positions/?{}'.format(bbox))
    assert json.loads(response.body) == {"data": []}


async def test_positions_returns_positions(pg_payload, client, bbox):
    payload = await pg_payload()
    response = await client.get('/positions/?{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert data[0]['coordinates'] == payload['coordinates']
    assert data[0]['time'] == int(payload['time'].timestamp())
    assert data[0]['registration'] == payload['registration']
    assert data[0]['allowed'] is True
    assert data[0]['registered'] is True


async def test_positions_returns_forbidden_positions(pg_payload, client, bbox):
    await pg_payload(allowed=False)
    response = await client.get('/positions/?{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert data[0]['allowed'] is False


async def test_positions_returns_only_registered_positions(pg_payload, client,
                                                           bbox):
    await pg_payload(registered=False)
    response = await client.get('/positions/?{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert not data


async def test_unchecked_positions_do_not_return_allowed_key(pg_payload,
                                                             client, bbox):
    await pg_payload(allowed_check_at=None)
    response = await client.get('/positions/?{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert 'allowed' not in data[0]


async def test_unregistered_positions_do_not_return_registered_key(pg_payload,
                                                                   client,
                                                                   bbox):
    await pg_payload(registered_check_at=None)
    response = await client.get('/positions/?{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert 'registered' not in data[0]


async def test_positions_filtered_by_allowance_true(pg_payload, client, bbox):
    await pg_payload()
    await pg_payload(allowed=False)
    await pg_payload(allowed=None)
    response = await client.get('/positions/?allowed=true&{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    assert data[0]['allowed'] is True


async def test_positions_filtered_by_allowance_false(pg_payload, client, bbox):
    await pg_payload()
    await pg_payload(allowed=False)
    await pg_payload(allowed=None)
    response = await client.get('/positions/?allowed=false&{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    assert data[0]['allowed'] is False


async def test_positions_filtered_by_allowance_unknown(pg_payload, client,
                                                       bbox):
    await pg_payload()
    await pg_payload(allowed=False)
    await pg_payload(allowed=None)
    response = await client.get('/positions/?allowed=null&{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    assert data[0]['allowed'] is None


async def test_positions_filtered_by_register_true(pg_payload, client, bbox):
    await pg_payload()
    await pg_payload(registered=False)
    await pg_payload(registered=None)
    response = await client.get('/positions/?registered=true&{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    assert data[0]['registered'] is True


async def test_positions_filtered_by_register_false(pg_payload, client, bbox):
    await pg_payload()
    await pg_payload(registered=False)
    await pg_payload(registered=None)
    response = await client.get('/positions/?registered=false&{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    assert data[0]['registered'] is False


async def test_positions_filtered_by_register_unknown(pg_payload, client,
                                                      bbox):
    await pg_payload()
    await pg_payload(registered=False)
    await pg_payload(registered=None)
    response = await client.get('/positions/?registered=null&{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    assert data[0]['registered'] is None


async def test_old_positions_are_not_retrieved(pg_payload, client, bbox):
    old = datetime.now(timezone.utc) - timedelta(seconds=31)
    await pg_payload(time=old)
    response = await client.get('/positions/?{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 0


async def test_fresh_positions_are_retrieved(pg_payload, client, bbox):
    fresh = datetime.now(timezone.utc) - timedelta(seconds=29)
    await pg_payload(time=fresh)
    response = await client.get('/positions/?{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1


async def test_tracks_are_retrieved(pg_payload, client, bbox):
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0))
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    items = data[payload['registration']]
    assert len(items) == 1
    assert items[0]['coordinates'] == payload['coordinates']
    assert items[0]['time'] == payload['time'].timestamp()
    assert items[0]['allowed'] is True
    assert items[0]['registered'] is True


async def test_tracks_are_retrieved_for_a_registration(pg_payload, client,
                                                       bbox):
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0))
    await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0), registration='bar')
    await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0))
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&registration={}&{}'.format(
            interval, payload['registration'], bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    items = data[payload['registration']]
    assert len(items) == 2


async def test_tracks_are_retrieved_by_allowed_unknown(pg_payload, client,
                                                       bbox):
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0))
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               allowed=False)
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               allowed=None)
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&allowed=null&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    items = data[payload['registration']]
    assert len(items) == 1
    assert items[0]['allowed'] is None


async def test_tracks_are_retrieved_by_allowance_true(pg_payload, client,
                                                      bbox):
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0))
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               allowed=False)
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               allowed=None)
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&allowed=1&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    items = data[payload['registration']]
    assert len(items) == 1
    assert items[0]['allowed'] is True


async def test_tracks_are_retrieved_by_allowance_false(pg_payload, client,
                                                       bbox):
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0))
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               allowed=False)
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               allowed=None)
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&allowed=off&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    items = data[payload['registration']]
    assert len(items) == 1
    assert items[0]['allowed'] is False


async def test_unallowed_tracks_are_retrieved(pg_payload, client, bbox):
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               allowed=False)
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    items = data[payload['registration']]
    assert len(items) == 1
    assert items[0]['allowed'] is False


async def test_unchecked_tracks_dont_return_allowed_key(pg_payload, client,
                                                        bbox):
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               allowed_check_at=None)
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    items = data[payload['registration']]
    assert len(items) == 1
    assert 'allowed' not in items[0]


async def test_tracks_are_retrieved_by_registered_unknown(pg_payload, client,
                                                          bbox):
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0))
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               registered=False)
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               registered=None)
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&registered=null&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    items = data[payload['registration']]
    assert len(items) == 1
    assert items[0]['registered'] is None


async def test_tracks_are_retrieved_by_registered_true(pg_payload, client,
                                                       bbox):
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0))
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               registered=False)
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               registered=None)
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&registered=1&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    items = data[payload['registration']]
    assert len(items) == 1
    assert items[0]['registered'] is True


async def test_tracks_are_retrieved_by_registered_false(pg_payload, client,
                                                        bbox):
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0))
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               registered=False)
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               registered=None)
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&registered=off&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    items = data[payload['registration']]
    assert len(items) == 1
    assert items[0]['registered'] is False


async def test_unregistered_tracks_are_not_retrieved(pg_payload, client, bbox):
    await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0), registered=False)
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert not data


async def test_unchecked_tracks_dont_return_registered_key(pg_payload, client,
                                                           bbox):
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               registered_check_at=None)
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    items = data[payload['registration']]
    assert len(items) == 1
    assert 'registered' not in items[0]


async def test_tracks_at_start_boundaries_are_retrieved(pg_payload, client,
                                                        bbox):
    payload = await pg_payload(time=datetime(2017, 3, 1, 13, 0, 0))
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    assert payload['registration'] in data


async def test_tracks_at_end_boundaries_are_retrieved(pg_payload, client,
                                                      bbox):
    payload = await pg_payload(time=datetime(2017, 3, 2, 13, 30, 0))
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    assert payload['registration'] in data


async def test_tracks_out_of_boundaries_arent_retrieved(pg_payload, client,
                                                        bbox):
    await pg_payload(time=datetime(2017, 3, 2, 13, 31, 0))
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&{}'.format(interval, bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 0


async def test_positions_one_position_per_registration(pg_payload, client,
                                                       bbox):
    now = datetime.now(timezone.utc)
    previous = now - timedelta(seconds=29)
    await pg_payload(time=previous, registration='same')
    await pg_payload(coordinates=[2.3666, 48.8666, 0],
                     time=now, registration='same')
    response = await client.get('/positions/?{}'.format(bbox))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    assert data[0]['coordinates'] == [2.3666, 48.8666, 0]


async def test_tracks_many_positions_per_registration(pg_payload, client):
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               coordinates=[1.5, 47.5, 0])
    payload2 = await pg_payload(time=datetime(2017, 3, 1, 14, 30, 0),
                                coordinates=[1.6, 47.6, 0])
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&west=1&south=47&east=2&north=48'.format(
            interval))
    data = json.loads(response.body)['data']
    assert payload['registration'] in data
    items = data[payload['registration']]
    assert items[0]['coordinates'] == payload['coordinates']
    assert items[1]['coordinates'] == payload2['coordinates']


async def test_tracks_shouldnt_consider_position_out_of_bbox(pg_payload,
                                                             client):
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               coordinates=[1.5, 47.5, 0])
    await pg_payload(time=datetime(2017, 3, 1, 14, 30, 0),
                     coordinates=[1.6, 47.6, 0])
    await pg_payload(time=datetime(2017, 3, 1, 14, 30, 0),
                     coordinates=[1.6, 48.6, 0])
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&west=1&south=47&east=2&north=48'.format(
            interval))
    data = json.loads(response.body)['data']
    assert payload['registration'] in data
    items = data[payload['registration']]
    assert len(items) == 2


async def test_tracks_many_positions_per_registration_except_time(pg_payload,
                                                                  client):
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               coordinates=[1.5, 47.5, 0])
    await pg_payload(time=datetime(2017, 3, 1, 14, 30, 0),
                     coordinates=[1.6, 47.6, 0])
    await pg_payload(time=datetime(2017, 3, 2, 14, 30, 0),
                     coordinates=[1.6, 47.7, 0])
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&west=1&south=47&east=2&north=48'.format(
            interval))
    data = json.loads(response.body)['data']
    assert payload['registration'] in data
    items = data[payload['registration']]
    assert len(items) == 2


async def test_tracks_should_group_positions_per_registration(pg_payload,
                                                              client):
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               coordinates=[1.5, 47.5, 0])
    await pg_payload(time=datetime(2017, 3, 1, 14, 30, 0),
                     coordinates=[1.6, 47.6, 0])
    await pg_payload(time=datetime(2017, 3, 1, 14, 30, 0),
                     coordinates=[1.6, 47.7, 0],
                     registration='test_bar')
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&west=1&south=47&east=2&north=48'.format(
            interval))
    data = json.loads(response.body)['data']
    assert payload['registration'] in data
    items = data[payload['registration']]
    assert len(items) == 2
    assert 'test_bar' in data
    assert len(data['test_bar']) == 1


async def test_bbox_positions_are_retrieved(pg_payload, client):
    await pg_payload(coordinates=[1.5, 47.5, 0])
    response = await client.get('/positions/?west=1&south=47&east=2&north=48')
    data = json.loads(response.body)['data']
    assert len(data) == 1


async def test_position_bbox_parameters_are_mandatory(pg_payload, client):
    await pg_payload(coordinates=[1.5, 47.5, 0])
    response = await client.get('/positions/?west=1&south=47&east=2')
    assert response.status == HTTPStatus.BAD_REQUEST
    assert 'A boundary box is required' in response.body


async def test_position_bbox_parameters_are_wrong(pg_payload, client):
    await pg_payload(coordinates=[1.5, 47.5, 0])
    response = await client.get('/positions/?west=1&south=47&east=2&north=100')
    assert response.status == HTTPStatus.BAD_REQUEST
    assert '`north` parameter must be within the [-90, 90]' in response.body


async def test_out_of_bbox_positions_are_not_retrieved(pg_payload, client):
    await pg_payload(coordinates=[2.5, 48.5, 0])
    response = await client.get('/positions/?west=1&south=47&east=2&north=48')
    data = json.loads(response.body)['data']
    assert len(data) == 0


async def test_bbox_tracks_requires_interval(pg_payload, client):
    await pg_payload(coordinates=[1.5, 47.5, 0])
    response = await client.get('/tracks/?west=1&south=47&east=2&north=48')
    assert response.status == HTTPStatus.BAD_REQUEST


async def test_tracks_within_bbox_are_retrieved(pg_payload, client):
    payload = await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                               coordinates=[1.5, 47.5, 0])
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&west=1&south=47&east=2&north=48'.format(
            interval))
    data = json.loads(response.body)['data']
    assert len(data) == 1
    assert payload['registration'] in data


async def test_tracks_without_bbox_are_not_retrieved(pg_payload, client):
    await pg_payload(time=datetime(2017, 3, 1, 14, 0, 0),
                     coordinates=[2.5, 48.5, 0])
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&west=1&south=47&east=2&north=48'.format(
            interval))
    data = json.loads(response.body)['data']
    assert len(data) == 0


async def test_tracks_off_boundaries_in_bbox_not_retrieved(pg_payload, client):
    await pg_payload(time=datetime(2017, 3, 2, 14, 0, 0),
                     coordinates=[1.5, 47.5, 0])
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    response = await client.get(
        '/tracks/?interval={}&west=1&south=47&east=2&north=48'.format(
            interval))
    data = json.loads(response.body)['data']
    assert len(data) == 0


async def test_tracks_interval_parameters_with_injection(pg_payload, client):
    resp = await client.get('/tracks/?interval=drop%20table%20data')
    assert resp.status == HTTPStatus.BAD_REQUEST
    assert '`interval` parameter must be a valid time interval' in resp.body


async def test_positions_bbox_parameters_with_injection(pg_payload, client):
    resp = await client.get(
        '/positions/?west=drop%20table%20data&south=47&east=2&north=48')
    assert resp.status == HTTPStatus.BAD_REQUEST
    assert resp.body == '{"error":"Key \'west\' must be castable to float"}'


async def test_tracks_bbox_parameters_with_injection(pg_payload, client):
    interval = '2017-03-01T13:00:00Z/2017-03-02T13:30:00Z'
    resp = await client.get((
        '/tracks/?interval={}'
        '&west=drop%20table%20data&south=47&east=2&north=48').format(interval))
    assert resp.status == HTTPStatus.BAD_REQUEST
    assert resp.body == '{"error":"Key \'west\' must be castable to float"}'


async def test_reports_invalid_hash(client):
    response = await client.get('/report/foo')
    assert response.status == HTTPStatus.BAD_REQUEST
    assert response.body == '{"error":"Invalid report hash"}'


async def test_reports_unknown_hash(client):
    response = await client.get('/report/{}'.format('*' * 32))
    assert response.status == HTTPStatus.NOT_FOUND
    assert response.body == '{"error":"Unknown report hash"}'


async def test_reports_with_error(client, report):
    key = '*' * 32
    await report(key=key, errors={'foo': 'bar'})
    response = await client.get('/report/{}'.format(key))
    assert response.status == HTTPStatus.OK
    payload = json.loads(response.body)
    assert payload['error'] == '{"foo": "bar"}'


async def test_report(client, report):
    await report(errors={'foo': 'bar'})
    response = await client.get('/report')
    assert response.status == HTTPStatus.OK
    payload = json.loads(response.body)
    assert payload['data'][0] == {
        'data': "b''",
        'error': '{"foo": "bar"}'
    }


async def test_report_return_20_reports_by_default(client, report):
    key = '*' * 30
    # We should only retrieve 20 reports among the 21 created.
    for num in range(0, 21):
        await report(key='{}{:02x}'.format(key, num), errors='{}'.format(num))
    response = await client.get('/report')
    assert response.status == HTTPStatus.OK
    payload = json.loads(response.body)
    # Verify that there are 20 reports returned
    assert len(payload['data']) == 20


async def test_report_return_last_reports_first(client, report):
    key = '*' * 31
    for num in range(0, 5):
        await report(key='{}{}'.format(key, num), errors='{}'.format(num))
    response = await client.get('/report')
    assert response.status == HTTPStatus.OK
    payload = json.loads(response.body)
    for num in range(0, 5):
        assert payload['data'][num]['error'] == '"{}"'.format(4 - num)


async def test_device_is_registered(client, connection, public_key, fetchrow):
    registration = 'tesuni123456789012fr'
    response = await client.put('/device/{}'.format(registration),
                                body=public_key.encode())
    assert response.status == HTTPStatus.CREATED
    device = await fetchrow(table_name='devices')
    assert device['registration'] == registration
    assert device['public_key'] == public_key


async def test_device_is_registered_with_upper(client, connection, public_key):
    registration = 'ABCDEF123456789012FR'
    response = await client.put('/device/{}'.format(registration),
                                body=public_key.encode())
    assert response.status == HTTPStatus.CREATED


async def test_device_registered_twice_should_return_400(client, connection,
                                                         public_key):
    registration = 'tesuni123456789012fr'
    response = await client.put('/device/{}'.format(registration),
                                body=public_key.encode())
    assert response.status == HTTPStatus.CREATED
    response = await client.put('/device/{}'.format(registration),
                                body=public_key.encode())
    assert response.status == HTTPStatus.BAD_REQUEST
    assert 'Device already registered.' in response.body


async def test_device_registered_same_key_should_return_400(client, connection,
                                                            public_key):
    registration = 'tesuni123456789012fr'
    response = await client.put('/device/{}'.format(registration),
                                body=public_key.encode())
    assert response.status == HTTPStatus.CREATED
    registration = 'tesuni123456789012de'
    response = await client.put('/device/{}'.format(registration),
                                body=public_key.encode())
    assert response.status == HTTPStatus.BAD_REQUEST
    assert 'Public key already registered.' in response.body


async def test_device_empty_public_key_return_400(client):
    registration = 'tesuni123456789012fr'
    response = await client.put('/device/{}'.format(registration), body=b'')
    assert response.status == HTTPStatus.BAD_REQUEST
    assert 'Public key is invalid.' in response.body


async def test_device_pubkey_without_header_return_400(client, public_key):
    registration = 'tesuni123456789012fr'
    response = await client.put('/device/{}'.format(registration),
                                body=public_key[40:].encode())
    assert response.status == HTTPStatus.BAD_REQUEST
    assert 'Public key is invalid.' in response.body


async def test_device_pubkey_truncated_header_return_400(client, public_key):
    registration = 'tesuni123456789012fr'
    response = await client.put('/device/{}'.format(registration),
                                body=public_key[1:].encode())
    assert response.status == HTTPStatus.BAD_REQUEST
    assert 'Public key is invalid.' in response.body


async def test_device_wrong_registration_format_return_400(client, public_key):
    registration = '*' * 20
    response = await client.put('/device/{}'.format(registration),
                                body=public_key.encode())
    assert response.status == HTTPStatus.BAD_REQUEST
    assert (json.loads(response.body)['error'] ==
            ('Registration format is invalid, '
             'it must match "^\w{6}\d{12}\w{2}$".'))
