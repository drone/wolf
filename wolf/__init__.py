import logging

logging.basicConfig(
    format='%(name)-15s - %(asctime)-15s - %(levelname)s: %(message)s')
