import asyncio
import socket
import socketserver
from datetime import datetime, timezone
from multiprocessing import Process
from pathlib import Path

import asyncpg
import jwt
import pytest
import ujson as json
from aiohttp.client_reqrep import ClientResponse
from postgis import Point
from postgis.asyncpg import register
from wolf import config as wolf_config
from wolf.__main__ import initdb
from wolf.api import app as myapp
from wolf.api.query import enforce_timezone
from wolf.config import ALGORITHM, DB_CONFIG, REDIS, UDP_HOST, UDP_PORT
from wolf.gate import Handler
from yarl import URL

DB_CONFIG['database'] = 'test_wolf'


def pytest_configure(config):
    asyncio.get_event_loop().run_until_complete(initdb())
    wolf_config.REGISTRATION_CACHE_TTL = 0


@pytest.fixture(scope='module')
def socket_client():
    return socket.socket(socket.AF_INET, socket.SOCK_DGRAM)


@pytest.fixture(scope='module')
def server():
    s = socketserver.UDPServer((UDP_HOST, UDP_PORT), Handler)
    p = Process(target=s.serve_forever)
    p.start()
    yield s.server_address
    p.terminate()


def pytest_runtest_teardown():
    REDIS.zremrangebyscore('queue', '-inf', 'inf')
    for key in REDIS.scan_iter('REG:*'):
        REDIS.delete(key)


# Default allowed positions
ALLOWED_POSITION = [43.57, 5.71, 0.0]


def pytest_runtest_setup():
    REDIS.zremrangebyrank('queue', 0, -1)

    async def main():
        conn = await asyncpg.connect(**DB_CONFIG)
        async with conn.transaction():
            await conn.execute('TRUNCATE TABLE data;')
            await conn.execute('TRUNCATE TABLE reports;')
            await conn.execute('TRUNCATE TABLE devices;')

    asyncio.get_event_loop().run_until_complete(main())


@pytest.fixture
def redis():
    return REDIS


@pytest.yield_fixture
async def redis_token(connection, register_device, private_key, public_key):

    async def insert_payload(**kwargs):
        data = {
            'time': datetime.now(timezone.utc).isoformat(),
            'registration': 'tesuni123456789012fr',
            'coordinates': ALLOWED_POSITION
        }
        data.update(kwargs)
        await register_device(data['registration'][:20], public_key)
        score = datetime.now(timezone.utc).timestamp()
        data = jwt.encode(data, private_key, algorithm=ALGORITHM)
        REDIS.zadd('queue', score, data)
        return data

    yield insert_payload


@pytest.fixture
def fetchrow(connection):

    async def _(table_name='data'):
        async with connection.transaction():
            result = await connection.fetchrow(
                'SELECT * FROM {} ORDER BY time DESC;'.format(table_name))
            return dict(result)

    return _


@pytest.fixture
def count(connection):

    async def _(table_name='data'):
        async with connection.transaction():
            return await connection.fetchval(
                'SELECT count(*) FROM {}'.format(table_name))

    return _


@pytest.fixture(scope='function')
async def mock_checks(monkeypatch):
    response = {'allowed': True, 'registered': True}

    async def request(self, method, url, **kwargs):
        url = URL(url)
        resp = ClientResponse(method, url)
        resp.headers = {'Content-Type': 'application/json'}
        resp._content = json.dumps(response).encode()
        resp.status = response['registered'] and 204 or 404
        return resp

    monkeypatch.setattr('aiohttp.ClientSession._request', request)

    def patcher(allowed=None, registered=None):
        if allowed is not None:
            response['allowed'] = allowed
        if registered is not None:
            response['registered'] = registered

    return patcher


@pytest.yield_fixture
async def connection():
    conn = await asyncpg.connect(**DB_CONFIG)
    await register(conn)
    yield conn
    await conn.close()


@pytest.yield_fixture
async def pg_payload(connection):
    async def insert_payload(**payload):
        default = {
            'coordinates': [2.3497, 48.8042, 0],  # Paris
            'time': datetime.now(timezone.utc),
            'registration': 'test_foo',
            'allowed': True,
            'allowed_check_at': datetime.now(timezone.utc),
            'registered': True,
            'registered_check_at': datetime.now(timezone.utc)
        }
        if 'time' in payload:
            payload['time'] = enforce_timezone(payload['time'])
        default.update(payload)
        point = Point(*default['coordinates'], srid=4326)
        time_ = default['time']
        registration = default['registration']
        allowed = default['allowed']
        allowed_check_at = default['allowed_check_at']
        registered = default['registered']
        registered_check_at = default['registered_check_at']
        await connection.execute(
            'INSERT INTO "data" '
            '    (coordinates, time, registration, allowed, allowed_check_at, '
            '     registered, registered_check_at) '
            'VALUES ($1, $2, $3, $4, $5, $6, $7)',
            point, time_, registration, allowed, allowed_check_at, registered,
            registered_check_at)
        return default
    yield insert_payload


@pytest.yield_fixture
async def report(connection):
    async def insert_report(errors, key='*' * 32):
        errors = json.dumps(errors)
        await connection.execute(
            'INSERT INTO "reports" (key, data, errors, time) '
            'VALUES ($1, $2, $3, $4)',
            key, b'', errors, datetime.now(timezone.utc))
    yield insert_report


@pytest.yield_fixture
async def register_device(connection, public_key):
    async def insert_device(registration, public_key=public_key):
        await connection.execute(
            'INSERT INTO "devices" (registration, public_key, time) '
            'VALUES ($1, $2, $3)',
            registration, public_key, datetime.now(timezone.utc))
    yield insert_device


@pytest.fixture
def bbox():
    france_metro_bbox = -4.99, 41.34, 9.69, 51.04
    return 'west={}&south={}&east={}&north={}'.format(*france_metro_bbox)


@pytest.fixture
def app():
    return myapp


@pytest.fixture
def private_key(path=Path.cwd() / 'tests' / 'keys' / 'rsa1024'):
    with path.open() as private_key_file:
        return private_key_file.read()


@pytest.fixture
def public_key(path=Path.cwd() / 'tests' / 'keys' / 'rsa1024.pub'):
    with path.open() as public_key_file:
        return public_key_file.read()
