# Registering devices public keys

## Motivations

Each sent position is signed with a private key, the corresponding public
key is required to be able to verify the signature and thus that the
message has not been tampered during the transmission (integrity).
Furthermore, it allows us to be certain of the authenticity of the
message.

Each and every device must be registered prior to send any position.
Otherwise, positions will be dropped.

## URL

`https://drone.api.gouv.fr/data/device/{registration}`

The `registration` part corresponds to the registration number of the device.

## Payload

You must send the payload through `PUT`.
It contains the public key string directly put within the body of the request.

That key must start with:

`-----BEGIN PUBLIC KEY-----`

## Response

If everything went fine, you should receive a `201` status code.

If that is not the case, a `400` status code will be returned and the body
will contain an explicit error message in JSON (check the `error` key).
