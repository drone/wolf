import asyncpg

from minicli import cli, run

from .config import DB_CONFIG
from .data import check_positions_conformity, data_process  # noqa: minicli
from .gate import serve_udp  # noqa: minicli
from .gate.proxy import serve_udp_proxy  # noqa: minicli
from .gate.client import udp_client  # noqa: minicli
from .api import serve_api  # noqa: minicli


@cli
async def initdb():
    """Initialize DB tables (data and reports)."""
    conn = await asyncpg.connect(**DB_CONFIG)
    async with conn.transaction():
        await conn.execute('CREATE EXTENSION IF NOT EXISTS postgis')
        await conn.execute(
            '''
            CREATE TABLE IF NOT EXISTS data (
                id serial PRIMARY KEY,
                coordinates geography(PointZ) NOT NULL,
                time timestamp with time zone NOT NULL,
                registration varchar(128) NOT NULL,
                allowed boolean NULL DEFAULT true,
                allowed_check_at timestamp with time zone NULL,
                registered boolean NULL DEFAULT true,
                registered_check_at timestamp with time zone NULL
            )
            ''')
        await conn.execute(
            '''
            CREATE TABLE IF NOT EXISTS reports (
                key varchar(32) PRIMARY KEY,
                data bytea NOT NULL,
                errors jsonb NOT NULL,
                time timestamp with time zone NOT NULL
            )
            ''')
        await conn.execute(
            '''
            CREATE TABLE IF NOT EXISTS devices (
                registration varchar(20) PRIMARY KEY,
                public_key text NOT NULL UNIQUE,
                time timestamp with time zone NOT NULL
            )
            ''')
    print('DB initialized')


if __name__ == '__main__':
    run()
