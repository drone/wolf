import json
import logging
from datetime import datetime, timedelta, timezone

import aiohttp
import jwt
from asyncpg.exceptions._base import PostgresError
from dateutil import parser

from .. import config
from ..config import (ALGORITHM, CHECK_CONFORMITY_URL, CHECK_REGISTRATION_URL,
                      LOGGING_LEVEL, REDIS, REGISTRATION_RE)

logger = logging.getLogger(__name__)
logger.setLevel(LOGGING_LEVEL)
mandatory_keys = {'time', 'registration', 'coordinates'}
validators = []


class TypedException(Exception):
    """Set the type attribute if any, default to `format`."""

    def __init__(self, *args, **kwargs):
        self.type = kwargs.pop('type', 'format')
        super().__init__(*args, **kwargs)


class FormatException(TypedException):
    """Raised if the payload format is invalid."""


class ContentException(TypedException):
    """Raised if the payload content is invalid."""


class UnregisteredException(TypedException):
    """Raised if the device is not registered (no public key)."""


def validator(func):
    # Warning, function order is important.
    validators.append(func)
    return func


async def validate(conn, position):
    errors = {}
    for validator in validators:
        try:
            position = await validator(conn, position)
        except (FormatException, UnregisteredException) as e:
            errors = position.errors
            errors.update({e.type: str(e)})
            return position._replace(errors=errors)
        except ContentException as e:
            errors = position.errors
            errors.update({e.type: str(e)})
            position = position._replace(errors=errors)
    return position


@validator
async def validate_payload(conn, position):
    token = position.token.decode().strip('\x00').strip()
    try:
        # This is a 2-steps process: first, we validate the payload is
        # decode-able (hence `verify=False`), then we check that
        # the signature is valid (within `validate_signature`) because
        # we want the `registration` key to be validated prior to that.
        payload = jwt.decode(token, verify=False)
    except jwt.exceptions.DecodeError:
        msg = 'Unreadable signed payload {}'.format(position.token)
        raise FormatException(msg)
    return position._replace(payload=payload)


@validator
async def validate_keys(conn, position):
    data_keys = position.payload.keys()
    if not mandatory_keys <= data_keys:
        msg = 'Missing mandatory keys {} in {}'.format(mandatory_keys,
                                                       position.payload)
        raise FormatException(msg)
    if len(data_keys) > len(mandatory_keys):
        msg = 'Too many keys {} in {}'.format(data_keys, position.payload)
        raise FormatException(msg)
    return position


@validator
async def validate_time(conn, position):
    payload = position.payload
    value = payload['time']
    try:
        if isinstance(value, (int, float)):
            value = datetime.utcfromtimestamp(value)
        else:
            value = parser.parse(value)
    except ValueError:
        raise ContentException('Invalid time "{}"'.format(value), type='time')
    if not value.tzinfo:
        value = value.replace(tzinfo=timezone.utc)
    now = datetime.now(timezone.utc)
    if not value <= now + timedelta(minutes=5):
        raise ContentException(
            ('Time is in the future (might be a timezone issue), '
             'time must always be in UTC. '
             'Received time {}, current one {}.').format(value, now),
            type='time')
    received_at = (datetime.utcfromtimestamp(position.received_at)
                           .replace(tzinfo=timezone.utc))
    latency = received_at - value
    if latency > timedelta(minutes=30):
        # Nicer formatting.
        latency = latency - timedelta(microseconds=latency.microseconds)
        raise ContentException(
            ('Time is in the past (might be a time synchronization issue). '
             'Latency is too high (> 30 minutes): {}. '
             'Received time {}, current one {}.').format(latency, value, now),
            type='time')
    payload['time'] = value
    return position._replace(payload=payload)


@validator
async def validate_registration(conn, position):
    payload = position.payload
    registration = str(payload['registration'])
    if not REGISTRATION_RE.match(registration):
        raise ContentException(
            ('Registration value is incorrect, it should match "{}"'
             ).format(REGISTRATION_RE.pattern), type='registration')
    payload['registration'] = registration
    return position._replace(payload=payload)


@validator
async def validate_coordinates(conn, position):
    payload = position.payload
    coordinates = payload['coordinates']
    if len(coordinates) != 3:
        raise FormatException(
            'Coordinates must have lat/lon/alt: "{}"'.format(coordinates),
            type='coordinates')
    lat, lon, alt = coordinates
    try:
        lat, lon, alt = float(lat), float(lon), float(alt)
    except ValueError:
        raise FormatException(
            'Coordinates must be floats: "{}"'.format(coordinates),
            type='coordinates')
    errors = []
    if not -90 < lat < 90:
        errors.append(
            'Latitude must be within [-90, 90]: {}'.format(coordinates))
    if not -180 < lon < 180:
        errors.append(
            'Longitude must be within [-180, 180]: {}'.format(coordinates))
    if not alt < 9000:
        # Because https://www.youtube.com/watch?v=SiMHTK15Pik
        errors.append("It's over 9000!!! {}".format(coordinates))
    if errors:
        raise FormatException(' '.join(errors), type='coordinates')
    payload['coordinates'] = [lat, lon, alt]
    return position._replace(payload=payload)


@validator
async def check_registration(conn, position):
    payload = position.payload
    key = 'REG:{}'.format(payload['registration'])
    url = CHECK_REGISTRATION_URL.format(payload['registration'])
    registered = REDIS.get(key)
    if registered is not None:
        payload['registered'] = bool(int(registered.decode()))
        payload['registered_check_at'] = datetime.now(timezone.utc)
        return position._replace(payload=payload)

    async with aiohttp.ClientSession() as session:
        try:
            async with session.head(url) as response:
                registered = response.status == 204
        except aiohttp.client_exceptions.ClientError:
            logger.error('Unable to process registration check: %s', url)
            payload['registered'] = None
            payload['registered_check_at'] = None
            # Do not cache into Redis temporary(?) server error on moose side.
        else:
            payload['registered'] = registered
            payload['registered_check_at'] = datetime.now(timezone.utc)
            REDIS.set(key, int(registered), config.REGISTRATION_CACHE_TTL)
    return position._replace(payload=payload)


@validator
async def validate_signature(conn, position):
    # Must be after `validate_registration`.
    if position.errors.get('registration'):
        # No need to check for signature if registration has errors.
        return position
    registration = position.payload['registration']
    try:
        async with conn.transaction():
            query = 'SELECT public_key FROM devices WHERE registration=$1'
            result = await conn.fetchrow(query, str(registration))
            if not result:
                raise UnregisteredException(
                    'Unknown device: no associated public key.',
                    type='registration')
    except (PostgresError, ValueError) as e:
        msg = 'Unknown public key from {}'.format(position.payload)
        raise FormatException(msg)
    try:
        jwt.decode(position.token, result['public_key'], algorithm=ALGORITHM)
    except jwt.exceptions.DecodeError:
        msg = 'Malformed signed payload {}'.format(position.payload)
        raise FormatException(msg)
    return position


@validator
async def check_conformity(conn, position):
    payload = position.payload
    url = CHECK_CONFORMITY_URL.format(*payload['coordinates'])
    async with aiohttp.ClientSession() as session:
        try:
            async with session.get(url) as response:
                data = await response.json()
        except (aiohttp.client_exceptions.ClientError,
                json.decoder.JSONDecodeError):
            logger.error('Unable to process conformity check: %s', url)
            payload['allowed'] = None
            payload['allowed_check_at'] = None
        else:
            payload['allowed'] = data['allowed']
            payload['allowed_check_at'] = datetime.now(timezone.utc)
    return position._replace(payload=payload)
