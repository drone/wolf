import logging
import os
import re

import redis

LOGGING_LEVEL = logging.DEBUG
DB_CONFIG = {
    'database': os.environ.get('POSTGRES_DB', 'wolf'),
    'host': os.environ.get('POSTGRES_HOST', None),
    'user': os.environ.get('POSTGRES_USER', None),
    'password': os.environ.get('POSTGRES_PASSWORD', None),
}

REDIS_DB = 0
REDIS_PORT = 6379
REDIS_HOST = os.environ.get('REDIS_HOST', 'localhost')
REDIS = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB)

CHECK_CONFORMITY_URL = (
    'https://drone.api.gouv.fr/flightareas/check?lon={0}&lat={1}&alt={2}')
CHECK_REGISTRATION_URL = 'https://drone.api.gouv.fr/account/device/{}'

API_HOST = os.environ.get('API_HOST', 'localhost')
API_PORT = 8080
UDP_HOST = os.environ.get('UDP_HOST', 'localhost')
UDP_PORT = 5005
PROXY_HOST = os.environ.get('PROXY_HOST', 'localhost')
PROXY_PORT = 5006

# Signature
ALGORITHM = 'RS512'

REGISTRATION_RE = re.compile('^\w{6}\d{12}\w{2}$')
REGISTRATION_CACHE_TTL = 60 * 30  # In seconds (average flight duration).
