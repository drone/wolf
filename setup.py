"Receive UDP or HTTP payloads from UAVs and expose those through an HTTP API."
from codecs import open  # To use a consistent encoding
from os import path

from setuptools import find_packages, setup

here = path.abspath(path.dirname(__file__))

# Get the long description from the relevant file
with open(path.join(here, 'README.md'), encoding='utf-8') as f:
    long_description = f.read()


def is_pkg(line):
    return line and not line.startswith(('-', 'git', '#'))


def load_packages(filepath):
    with open(filepath, encoding='utf-8') as reqs:
        return [l for l in reqs.read().split('\n') if is_pkg(l)]


VERSION = (0, 1, 0)

__author__ = 'Yohan Boniface'
__contact__ = "yohan.boniface@data.gouv.fr"
__homepage__ = "https://framagit.org/drone/wolf"
__version__ = ".".join(map(str, VERSION))

setup(
    name='wolf',
    version=__version__,
    description=__doc__,
    long_description=long_description,
    url=__homepage__,
    author=__author__,
    author_email=__contact__,
    license='MIT',

    # See https://pypi.python.org/pypi?%3Aaction=list_classifiers
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Developers',
        'Topic :: Internet :: WWW/HTTP :: HTTP Servers',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
    ],
    keywords='uavs drones real-time udp api',
    packages=find_packages(exclude=['tests']),
    install_requires=load_packages('requirements.txt'),
    extras_require={
        'contributing': load_packages('requirements-dev.txt'),
        'docs': 'mkdocs'
    },
    include_package_data=True,
    entry_points={
        'console_scripts': ['wolf=wolf.__main__:run'],
    },
)
