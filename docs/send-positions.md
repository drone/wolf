# Sending positions

## Initial payloads

```json
{
    "registration": "registration number",
    "time": "ISO 8601:2004 as UTC timezone",
    "coordinates": [lon, lat, alt]
}
```

For instance:

```json
{
    "registration": "ABCDEFG",
    "time": "2017-06-14T13:11:51+00:00",
    "coordinates": [2.7905, 48.7517, 131]
}
```

*Beware:* the `time` sent must be in UTC and match the server’s clock.
You can check it via the `Date` header:
`curl --head https://drone.api.gouv.fr/ | grep Date`


## Sign payloads

Payloads are signed using
[JSON Web Signature (JWS)](https://tools.ietf.org/html/rfc7515).

Once signed with your private key, payloads will be likewise
(striped down to fit in a screen but that might be way longer):

`eyJ0eXAiOi[strip]UxMiJ9.eyJ0aW1lIjoi[strip]NyLDAuMF19.P-vvxZb[strip]MDU5NKt-Q`

You have to send your public key first to our API
(see the “Register devices” documentation).


## Send UDP payloads

* IP address: `drone.api.gouv.fr`
* Port: `5005`


## Send HTTP payloads (experimental proxy)

`https://drone.api.gouv.fr/proxy/`

Payloads are the same as the ones sent through UDP.
Send them using the POST HTTP verb.
