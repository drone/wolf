import jwt
from wolf.config import ALGORITHM, REDIS


def test_response_ok(socket_client, server):
    socket_client.sendto(b'foo', server)
    result = socket_client.recv(1024)
    assert (b'Accepted, optional error report: '
            b'https://drone.api.gouv.fr/data/report/') in result


def test_redis_zset(socket_client, server):
    socket_client.sendto(b'foo', server)
    socket_client.recv(1024)  # Wait for response to be processed.
    assert REDIS.zrange('queue', 0, -1) == [b'foo']


def test_payload_is_striped(socket_client, server):
    socket_client.sendto(b' foo\n', server)
    socket_client.recv(1024)  # Wait for response to be processed.
    assert REDIS.zrange('queue', 0, -1) == [b'foo']


def test_real_payload(socket_client, server, private_key, public_key):
    payload = {
        'registration': 'ABCDEFG',
        'time': '2017-06-14T13:11:51+00:00',
        'coordinates': [48.7517, 2.7905, 131]
    }
    data = jwt.encode(payload, private_key, algorithm=ALGORITHM)
    socket_client.sendto(data, server)
    socket_client.recv(1024)  # Wait for response to be processed.
    data2 = REDIS.zrange('queue', 0, -1)
    assert len(data2) == 1
    payload2 = jwt.decode(data2[0], public_key, algorithm=ALGORITHM)
    assert payload == payload2
