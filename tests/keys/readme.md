See https://www.madboa.com/geek/openssl/#keys
    https://stackoverflow.com/q/5244129
Private: openssl genrsa -out tests/keys/rsa1024 1024
Public: openssl rsa -in tests/keys/rsa1024 -out tests/keys/rsa1024.pub -pubout
