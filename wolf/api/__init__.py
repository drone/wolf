import asyncio
import logging
from collections import defaultdict
from datetime import datetime, timezone
from http import HTTPStatus
from pathlib import Path

import uvloop
from asyncpg import create_pool
from asyncpg.exceptions import UniqueViolationError
from asyncpg.exceptions._base import PostgresError
from minicli import cli
from OpenSSL.crypto import FILETYPE_PEM, Error, load_publickey
from postgis.asyncpg import register
from roll import HttpError, Protocol, Roll
from roll.extensions import cors, simple_server, traceback

from ..config import (API_HOST, API_PORT, DB_CONFIG, LOGGING_LEVEL,
                      REGISTRATION_RE)
from .query import OwlQuery

logger = logging.getLogger(__name__)
logger.setLevel(LOGGING_LEVEL)


def load_sql_query(name):
    path = Path(__file__).parent / 'sql' / name
    with path.open() as f:
        return f.read()


POSITIONS_PER_REGISTRATION = load_sql_query('positions_per_registration.sql')
TRACKS_FOR = load_sql_query('tracks_for.sql')


class OwlProtocol(Protocol):
    Query = OwlQuery


class OwlRoll(Roll):
    Protocol = OwlProtocol


asyncio.set_event_loop(uvloop.new_event_loop())
app = OwlRoll()
cors(app, methods=['GET', 'OPTIONS', 'PUT'])
traceback(app)


def jsonify(records):
    """
    Parse asyncpg record response into JSON format
    """
    yield from (dict(r.items()) for r in records)


@app.listen('startup')
async def register_db():
    app.pool = await create_pool(**DB_CONFIG, loop=app.loop, init=register)


@app.listen('shutdown')
async def close_connection():
    await app.pool.close()


@app.listen('error')
async def on_error(request, response, error):
    response.status = error.status
    response.json = {'error': error.message}


def filter_keys(payload, excludes):
    if not payload['allowed_check_at']:
        del payload['allowed']
    if not payload['registered_check_at']:
        del payload['registered']
    for key in excludes:
        del payload[key]
    return payload


@app.route('/positions/')
async def positions(request, response):
    async with app.pool.acquire() as connection:
        query = POSITIONS_PER_REGISTRATION.format(
            allowed=request.query.allowed,
            registered=request.query.registered)
        results = await connection.fetch(query, *request.query.bbox)
        results = [filter_keys(payload, excludes=['allowed_check_at',
                                                  'registered_check_at'])
                   for payload in jsonify(results)]
        response.json = {'data': results}


@app.route('/tracks/')
async def tracks(request, response):
    async with app.pool.acquire() as connection:
        query = TRACKS_FOR.format(interval=request.query.interval,
                                  allowed=request.query.allowed,
                                  registered=request.query.registered,
                                  registration=request.query.registration)
        results = await connection.fetch(query, *request.query.bbox)
        data = defaultdict(list)
        for result in results:
            data[result['registration']].append(
                filter_keys(dict(result),
                            excludes=['allowed_check_at',
                                      'registered_check_at', 'registration']))
        response.json = {'data': data}


@app.route('/report/{error_hash}')
async def report(request, response, error_hash):
    if len(error_hash) != 32:
        raise HttpError(HTTPStatus.BAD_REQUEST, 'Invalid report hash')
    async with app.pool.acquire() as connection:
        report = await connection.fetchrow(
            'SELECT errors, data FROM reports WHERE key=$1', error_hash)
        if not report:
            raise HttpError(HTTPStatus.NOT_FOUND, 'Unknown report hash')
        response.json = {
            'error': report['errors'],
            'data': bytes(report['data']).decode(errors='ignore')}


@app.route('/report')
async def reports(request, response):
    async with app.pool.acquire() as connection:
        reports = await connection.fetch(
            'SELECT errors, data FROM reports ORDER BY time DESC LIMIT 20')
        result = [{'error': str(report['errors']),
                   'data': str(report['data'])} for report in reports]
        response.json = {'data': result}


@app.route('/device/{registration}', methods=['PUT'])
async def register_device(request, response, registration):
    if not REGISTRATION_RE.match(registration):
        message = ('Registration format is invalid, it must match "{}".'
                   ).format(REGISTRATION_RE.pattern)
        raise HttpError(HTTPStatus.BAD_REQUEST, message)
    public_key = request.body
    try:
        load_publickey(FILETYPE_PEM, public_key.decode())
    except Error:
        raise HttpError(HTTPStatus.BAD_REQUEST, 'Public key is invalid.')
    try:
        async with app.pool.acquire() as connection:
            logger.info('Saving public key: %s (%s)', registration, public_key)
            await connection.execute(
                '''
                INSERT INTO "devices" (registration, public_key, time)
                VALUES ($1, $2, $3)
                ''',
                registration, public_key.decode(), datetime.now(timezone.utc))
        response.status = HTTPStatus.CREATED
    except UniqueViolationError as e:
        logger.error('%s => %s (%s)', e, registration, public_key)
        if 'devices_public_key_key' in e.message:
            message = 'Public key already registered.'
        else:
            message = 'Device already registered.'
        raise HttpError(HTTPStatus.BAD_REQUEST, message)
    except (PostgresError, ValueError) as e:
        logger.error('%s => %s (%s)', e, registration, public_key)
        raise HttpError(HTTPStatus.SERVER_ERROR)


@cli
def serve_api():
    """Run API development server."""
    logger.info('Running API on http://%s:%s', API_HOST, API_PORT)
    simple_server(app, host=API_HOST, port=API_PORT, quiet=True)
