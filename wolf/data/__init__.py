import asyncio
import hashlib
import json
import logging
from collections import namedtuple
from datetime import datetime, timezone

import asyncpg
from asyncpg.exceptions._base import PostgresError
from minicli import cli
from postgis import Point
from postgis.asyncpg import register

from ..config import DB_CONFIG, LOGGING_LEVEL, REDIS
from .validators import check_conformity, check_registration, validate

logger = logging.getLogger(__name__)
logger.setLevel(LOGGING_LEVEL)

WSG84 = 4326


class Position(namedtuple('Position',
                          ['token', 'received_at', 'payload', 'errors'])):
    __slots__ = ()  # Keep it for performances.

    def __new__(cls, token, received_at, payload=None, errors=None):
        """Allow default mutables for payload and errors."""
        return super(Position, cls).__new__(
            cls, token, received_at, payload or {}, errors or {})


async def process_queue(conn):
    data = REDIS.zrange('queue', 0, -1, withscores=True)
    await parse_positions(conn, data)
    REDIS.zremrangebyrank('queue', 0, len(data) - 1)


async def parse_positions(conn, positions):
    for token, received_at in positions:
        position = Position(token, received_at)
        logger.info('Processing position: %s', position)
        position = await validate(conn, position)
        if position.errors:
            await save_reports(conn, position)
        else:
            await save_payload(conn, position.payload)


async def save_payload(conn, payload):
    point = Point(*payload['coordinates'], srid=WSG84)
    time_ = payload['time']
    registration = payload['registration']
    allowed = payload['allowed']
    allowed_check_at = payload['allowed_check_at']
    registered = payload['registered']
    registered_check_at = payload['registered_check_at']
    try:
        async with conn.transaction():
            logger.info('Saving payload: %s (%s)', registration, point)
            await conn.execute(
                '''
                INSERT INTO "data"
                    (coordinates, time, registration,
                     allowed, allowed_check_at,
                     registered, registered_check_at)
                VALUES ($1, $2, $3, $4, $5, $6, $7)
                ''',
                point, time_, registration, allowed, allowed_check_at,
                registered, registered_check_at)
    except PostgresError as e:
        logger.error('%s => %s', e, payload)


async def save_reports(conn, position):
    key = hashlib.md5(position.token).hexdigest()
    time_ = (datetime.utcfromtimestamp(position.received_at)
                     .replace(tzinfo=timezone.utc))
    errors = json.dumps(position.errors)
    try:
        async with conn.transaction():
            logger.info(
                'Saving report: %s (%s) => %s', key, position.payload, errors)
            await conn.execute(
                '''
                INSERT INTO "reports" (key, data, errors, time)
                VALUES ($1, $2, $3, $4)
                ON CONFLICT (key) DO UPDATE
                SET data=excluded.data, errors=excluded.errors,
                time=excluded.time
                ''',
                key, position.token, errors, time_)
    except (PostgresError, ValueError) as e:
        logger.error('%s => %s', e, position.token.decode(errors='ignore'))


@cli
async def check_positions_conformity():
    """(Re)check all unchecked positions against skunk."""
    conn = await asyncpg.connect(**DB_CONFIG)
    await register(conn)
    async with conn.transaction():
        updates = 0
        query = 'SELECT * FROM data WHERE allowed_check_at IS NULL'
        for record in await conn.fetch(query):
            position = Position(None, None, {'coordinates': record[1]}, {})
            position = await check_conformity(conn, position)
            allowed = position.payload['allowed']
            allowed_check_at = position.payload['allowed_check_at']
            if allowed_check_at is not None:
                await conn.execute(
                    '''
                    UPDATE data
                    SET allowed=$1, allowed_check_at=$2
                    WHERE id=$3
                    ''',
                    allowed, allowed_check_at, record[0])
                updates += 1
        print('{} records conformity checked.'.format(updates))


@cli
async def check_positions_registration():
    """(Re)check all unchecked positions against moose."""
    conn = await asyncpg.connect(**DB_CONFIG)
    await register(conn)
    async with conn.transaction():
        updates = 0
        query = 'SELECT * FROM data WHERE registered_check_at IS NULL'
        for record in await conn.fetch(query):
            position = Position(None, None, {'coordinates': record[3]}, {})
            position = await check_registration(conn, position)
            registered = position.payload['registered']
            registered_check_at = position.payload['registered_check_at']
            if registered_check_at is not None:
                await conn.execute(
                    '''
                    UPDATE data
                    SET registered=$1, registered_check_at=$2
                    WHERE id=$3
                    ''',
                    registered, registered_check_at, record[0])
                updates += 1
        print('{} records registration checked.'.format(updates))


@cli
async def data_process():
    """Process and validate Redis payloads and insert into Postgres."""
    logger.info('Running data processing')
    conn = await asyncpg.connect(**DB_CONFIG)
    await register(conn)
    while 1:
        await process_queue(conn)
        await asyncio.sleep(1)
