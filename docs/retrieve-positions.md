# Retrieving positions

The goal of this API is to be able to retrieve positions given
a boundary box and a time lapse. A few filters are available to refine
returned positions in order to match your needs, feel free to
[contact us](https://framateam.org/drone/) if you need
more fine grained queries.


## Retrieve recent positions

`https://drone.api.gouv.fr/data/positions/`

Only positions active in the last 30 seconds are visible. And only
one position per registration identifier (unique per drone).

### Required GET parameters

A bounding box is required to filter positions
(`west`, `south`, `east` and `north`) in WGS84 format.

#### Example

`https://drone.api.gouv.fr/data/positions/?west=5.7449376583&south=43.5379871576&east=5.7614278793&north=43.5474283332`


### Payload

```json
{

    "data": [
        {
            "registration": "20170608105825",
            "time": 1498058072,
            "coordinates": [
                -9.461722,
                38.8281952,
                0.0
            ],
            "allowed": <optional>,
            "registered": <optional>
        }
    ]

}
```

The payload may return an optional `allowed` key per position
if it has been checked against the geo-fencing service. The value can
be either `true` (allowed), `false` (not allowed) or `null` (unknown).

The payload may return an optional `registered` key per position
if it has been checked against the registration service. The value can
be either `true` (registered), `false` (not registered) or `null` (unknown).


### Optional GET parameters

It is possible to combine these parameters to refine your query.

#### `allowed`

You can filter returned positions by adding the `allowed` GET parameter
to your request. It can be either `true`, `false` or `null`.

The `allowed` status depends on the conformity of the position.


#### `registered`

You can filter returned positions by adding the `registered` GET parameter
to your request. It can be either `true`, `false` or `null`.

The `registered` status depends on the existence of the device.


## Retrieve past tracks

`https://drone.api.gouv.fr/data/tracks/`

Returns tracks for a given interval of time and bounding box.

### Required GET parameters

A bounding box is required to filter tracks
(`west`, `south`, `east` and `north`) in WGS84 format.

An interval is required to filter tracks (`interval`) using
[ISO 8601](https://en.wikipedia.org/wiki/ISO_8601#Time_intervals).
Passed datetimes must be in UTC.

#### Example

`https://drone.api.gouv.fr/data/tracks/?west=5.7449376583&south=43.5379871576&east=5.7614278793&north=43.5474283332&interval=2017-03-01T13:00:00Z/2017-03-02T13:30:00Z`


#### Payload

```json
{
    "data": {
        "<registration_numberXYZ>": [
            {
                "time": 1498058072,
                "coordinates": [
                    -9.461722,
                    38.8281952,
                    0.0
                ],
                "allowed": <optional>,
                "registered": <optional>
            },
            ...
        ],
        "<registration_numberABC>": [{...}, ...],
        ...
    }
}
```

The payload may return an optional `allowed` key per position
if it has been checked against the geo-fencing service. The value can
be either `true` (allowed), `false` (not allowed) or `null` (unknown).

The payload may return an optional `registered` key per position
if it has been checked against the registration service. The value can
be either `true` (registered), `false` (not registered) or `null` (unknown).

### Optional GET parameters

It is possible to combine these parameters to refine your query.

#### `allowed` (default: all positions)

You can filter returned positions by adding the `allowed` GET parameter
to your request. It can be either `true`, `false` or `null`.

The `allowed` status depends on the conformity of the position.


#### `registered` (default: `true`)

You can filter returned positions by adding the `registered` GET parameter
to your request. It can be either `true`, `false` or `null`.

The `registered` status depends on the registration to the national authority.


#### `registration` (default: all positions)

You can filter returned positions by adding the `registration` GET parameter
to your request. In that case, only one registration key will be available
in the response for the given boundary box and interval.
