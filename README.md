# Wolf

Receive UDP or HTTP payloads from UAVs and expose those through an HTTP API.

For usage as a consumer of APIs, check https://drone.beta.gouv.fr/developers/

## Requirements

* [Python](https://www.python.org/) 3.5+
* [PostgreSQL](https://www.postgresql.org/)
* [Redis](https://redis.io/)

## Contributing

### Big picture

There are three main components:

* the gate handles incoming UDP payloads and store those in Redis
* the data processor checks validity of payloads and stores those in PostgreSQL
* the api exposes the validated data through an HTTP API

Optionnaly, a HTTP proxy is made available if payloads cannot be send
through UDP.

### Setup (only once)

1. Install [git](https://git-scm.com/)
2. Clone the repository (`git clone git@framagit.org:drone/wolf.git`) and cd
   inside it (`cd wolf`).
3. Create and activate a dedicated
   [virtualenv](http://docs.python-guide.org/en/latest/dev/virtualenvs/)
4. Run `make develop`

### Databases (only once)

1. Adapt the `wolf/config.py` file to match your system (note that you
   can use environment variables too)
2. Run Redis and PostgreSQL (launchers depends on your system)
3. Run `make setup_db`

### Development

1. Launch all services `make -j4 run`
2. Open a new terminal session (in same directory and with virtualenv activated)
   and send a dummy position to process: `make run_client`
3. Logs should not return any error within your first terminal session
4. Test that your position is available by running (within 30 seconds)
   `http :8080/positions north==50 south==10 west==0 east==8`
5. Hack! (see workflow below)

Run the `make help` command for up-to-date low level documentation.

### Workflow

We develop using merge-requests (pull-requests if you are familiar with
Github) to be able to peer-review each and every change. It is
recommended to use a dedicated branch for that.

Feel free to join us on https://framateam.org/drone/ to chat about that!
