import hashlib
import json
import random
import socket
import sys
import time
from datetime import datetime, timezone

from minicli import cli

from ..config import UDP_HOST, UDP_PORT


def send_data(data, host, verbose=True):
    bytes_data = bytes(data, 'utf-8')
    error_hash = hashlib.md5(bytes_data).hexdigest()
    if verbose:
        print('Sending: {} to {}'.format(data, host))
        print('Error hash: {} (useful to debug)'.format(error_hash))

    # SOCK_DGRAM is the socket type to use for UDP sockets
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # As you can see, there is no connect() call; UDP has no connections.
    # Instead, data is directly sent to the recipient via sendto().
    sock.sendto(bytes_data, (host, UDP_PORT))
    received = str(sock.recv(1024), 'utf-8')

    if verbose:
        print('Valid connection to {}'.format(host))
        print('Received: {} from {}'.format(received, host))


@cli('bbox', nargs=4)
def udp_client(payload: str=None, host=UDP_HOST,
               bbox=[-4.99, 41.34, 9.69, 51.04], count=1, tracks=1):
    """Send valid payload(s) to a UDP host.

    :payload:   optional payload to send (Default: generated payload)
    :host:      IP to send the payload to
    :bbox:      restrict the generated lon/lat to this bbox (Default: France)
    :count:     number of points per simulated UAV flight
    :tracks:    number of UAV flights to simulate
    """

    # Payload takes precedence over other parameters.
    if payload:
        send_data(payload, host)
        sys.exit()

    positions = {}
    hostname = socket.gethostname()
    for t in range(tracks):
        for c in range(count):
            registration = '{hostname}{c}'.format(hostname=hostname, c=c)
            timestamp = datetime.now(timezone.utc).isoformat()
            elevation = random.randint(0, 150)
            if positions.get(registration):
                # On metropolitan France area, one degree is around 80 km,
                # the given range allows a drone to fly at 100 km/h max.
                lat = positions[registration][0] + random.uniform(-0.001, .001)
                lon = positions[registration][1] + random.uniform(-0.001, .001)
            else:
                lat = random.uniform(float(bbox[0]), float(bbox[2]))
                lon = random.uniform(float(bbox[1]), float(bbox[3]))
            data = {
                'registration': registration,
                'time': timestamp,
                'coordinates': [lat, lon, elevation]
            }
            positions[registration] = [lat, lon]
            send_data(json.dumps(data), host, verbose=False)

        time.sleep(3)
