# Order of commands will be respected by the help command.

develop:  ## Install python dev dependencies.
	python setup.py develop
	pip install wolf[contributing]

run: run_udp run_udp_proxy data_process run_api ## Run all, must be called with `make -j4 run`.

run_udp: ## Run the UDP server to store payloads into Redis.
	wolf serve_udp

run_udp_proxy: ## Run the HTTP to UDP server.
	wolf serve_udp_proxy

data_process: ## Process incoming data from Redis to PostGIS.
	wolf data_process

run_api: ## Run the HTTP API to query positions, tracks and reports.
	wolf serve_api

run_client: ## Run the test client to send dummy positions.
	wolf udp_client

setup_db: ## Setup PostgreSQL database.
	createdb wolf || exit 0
	createdb test_wolf || exit 0
	wolf initdb

update:  ## Update python dependencies.
	pip install -Ur requirements-dev.txt

docs: ## Serve documentation in HTML.
	mkdocs serve

# See https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
.PHONY: help docs

help: ## Print all commands and associated documentation.
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-15s\033[0m %s\n", $$1, $$2}'

.DEFAULT_GOAL := help
