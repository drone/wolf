import hashlib
import logging
import socketserver
from datetime import datetime, timezone

from minicli import cli

from ..config import LOGGING_LEVEL, UDP_HOST, UDP_PORT, REDIS

logger = logging.getLogger(__name__)
logger.setLevel(LOGGING_LEVEL)


class Handler(socketserver.BaseRequestHandler):

    def handle(self):
        data = self.request[0].strip()
        logger.info('Receiving data: %s', data)
        socket = self.request[1]
        score = datetime.now(timezone.utc).timestamp()
        REDIS.zadd('queue', score, data)
        key = hashlib.md5(data).hexdigest()
        url = 'https://drone.api.gouv.fr/data/report/{}'.format(key)
        content = 'Accepted, optional error report: {}'.format(url)
        socket.sendto(bytes(content, 'utf-8'), self.client_address)


@cli
def serve_udp():
    """Receive payloads from UAVs."""
    # Use `with` if Python 3.6 support only.
    logger.info('Running UDP on http://%s:%s', UDP_HOST, UDP_PORT)
    server = socketserver.UDPServer((UDP_HOST, UDP_PORT), Handler)
    server.serve_forever()
