import json
import time
from datetime import datetime, timedelta, timezone

import jwt
import pytest
from aiohttp import ClientSession
from conftest import ALLOWED_POSITION
from wolf.config import ALGORITHM
from wolf.data import Position, check_positions_conformity, process_queue
from wolf.data.validators import validate

pytestmark = pytest.mark.asyncio


async def test_simple_payload(redis_token, count, fetchrow, mock_checks,
                              connection):
    now = datetime.now(timezone.utc)
    registration = 'tesuni123456789012fr'
    await redis_token(time=now.isoformat(), registration=registration,
                      coordinates=ALLOWED_POSITION)
    await process_queue(connection)
    assert await count() == 1
    result = await fetchrow()
    assert result['time'] == now
    assert result['registration'] == registration
    assert result['coordinates'].coords == (43.57, 5.71, 0)
    assert await count('reports') == 0


async def test_upper_registration(redis_token, count, connection):
    now = datetime.now(timezone.utc)
    registration = 'ABCDEF123456789012FR'
    await redis_token(time=now.isoformat(), registration=registration,
                      coordinates=ALLOWED_POSITION)
    await process_queue(connection)
    assert await count() == 1


async def test_can_pass_timestamp_as_time(redis_token, count, fetchrow,
                                          mock_checks, connection):
    now = datetime.now(timezone.utc)
    await redis_token(time=now.timestamp())
    await process_queue(connection)
    assert await count() == 1
    result = await fetchrow()
    assert result['time'] == now
    assert await count('reports') == 0


async def test_unparsable_time_doesnt_crash(redis_token, count, fetchrow,
                                            mock_checks, connection,
                                            public_key):
    await redis_token(time='not a date')
    await process_queue(connection)
    assert await count() == 0
    assert await count('reports') == 1
    error = await fetchrow('reports')
    payload = jwt.decode(error['data'], public_key, algorithm=ALGORITHM)
    assert payload['time'] == 'not a date'
    assert 'Invalid time "not a date"' in json.loads(error['errors'])['time']


async def test_missing_time_does_not_crash(redis, count, fetchrow, connection,
                                           register_device, private_key,
                                           public_key):
    payload = {
        'registration': 'tesuni123456789012fr',
        'coordinates': [1, 2, 3]
    }
    await register_device(payload['registration'])
    data = jwt.encode(payload, private_key, algorithm=ALGORITHM)
    redis.zadd('queue', 123456789, data)
    await process_queue(connection)
    assert await count() == 0
    assert await count('reports') == 1
    error = await fetchrow('reports')
    payload = jwt.decode(error['data'], public_key, algorithm=ALGORITHM)
    assert 'registration' in payload
    assert 'Missing mandatory keys' in json.loads(error['errors'])['format']


async def test_invalid_json_does_not_crash(redis, count, fetchrow, connection):
    redis.zadd('queue', 123456789, '{')
    await process_queue(connection)
    assert await count() == 0
    assert await count('reports') == 1
    error = await fetchrow('reports')
    assert bytes(error['data']) == b'{'
    assert (json.loads(error['errors'])['format'] ==
            "Unreadable signed payload b'{'")


async def test_invalid_twice_create_unique_error(redis, count, connection):
    redis.zadd('queue', 123456789, '{')
    redis.zadd('queue', 123456789, '{')
    await process_queue(connection)
    assert await count('reports') == 1


async def test_validate_invalid_payload(connection):
    position = Position(b'foo', None)
    position = await validate(connection, position)
    assert 'Unreadable signed payload' in position.errors['format']


async def test_validate_missing_keys(connection, private_key):
    token = jwt.encode({'coordinates': []}, private_key, algorithm=ALGORITHM)
    position = Position(token, 1)
    position = await validate(connection, position)
    assert 'Missing mandatory keys' in position.errors['format']


async def test_validate_too_many_extra_keys(connection, register_device,
                                            private_key):
    with_extra_keys = {
        'coordinates': [],
        'registration': '1234',
        'time': '',
        'foo': ''
    }
    await register_device(with_extra_keys['registration'])
    token = jwt.encode(with_extra_keys, private_key, algorithm=ALGORITHM)
    position = Position(token, 1)
    position = await validate(connection, position)
    assert 'Too many keys' in position.errors['format']


async def test_validate_invalid_time(redis_token, mock_checks, connection):
    token = await redis_token(**{'time': 'foo'})
    position = Position(token, 1)
    position = await validate(connection, position)
    assert 'Invalid time "foo"' in position.errors['time']


async def test_validate_future_time(redis_token, mock_checks, connection):
    token = await redis_token(**{
        'time': (datetime.now(timezone.utc) +
                 timedelta(seconds=301)).isoformat()
    })
    position = Position(token, 1)
    position = await validate(connection, position)
    assert ('Time is in the future (might be a timezone issue'
            in position.errors['time'])


async def test_validate_past_time(redis_token, mock_checks, connection):
    now = datetime.now(timezone.utc)
    token = await redis_token(**{
        'time': (now - timedelta(minutes=31)).isoformat(),
    })
    received_at = now.timestamp()
    position = Position(token, received_at)
    position = await validate(connection, position)
    assert 'Time is in the past' in position.errors['time']


async def test_validate_registration_not_conform(redis_token, mock_checks,
                                                 connection):
    token = await redis_token(**{'registration': '©©©©©©123456789012fr'})
    position = Position(token, 1)
    position = await validate(connection, position)
    assert ('should match "^\\w{6}\\d{12}\\w{2}$"'
            in position.errors['registration'])


async def test_validate_coordinates_invalid(redis_token, connection):
    token = await redis_token(**{'coordinates': [1, 2]})
    position = Position(token, 1)
    position = await validate(connection, position)
    assert ('Coordinates must have lat/lon/alt'
            in position.errors['coordinates'])


async def test_validate_coordinates_invalid_not_float(redis_token,
                                                      connection):
    token = await redis_token(**{'coordinates': ['1A', 2, 3]})
    position = Position(token, 1)
    position = await validate(connection, position)
    assert 'Coordinates must be floats' in position.errors['coordinates']


async def test_validate_coordinates_as_strings(redis_token, mock_checks,
                                               connection):
    token = await redis_token(**{'coordinates': ['43.57', '5.71', '0.0']})
    position = Position(token, 1)
    position = await validate(connection, position)
    assert 'coordinates' not in position.errors
    assert position.payload['coordinates'] == ALLOWED_POSITION


async def test_validate_coordinates_out_of_range_latitude(redis_token,
                                                          connection):
    token = await redis_token(**{'coordinates': [91, 0, 0]})
    position = Position(token, 1)
    position = await validate(connection, position)
    assert ('Latitude must be within [-90, 90]'
            in position.errors['coordinates'])


async def test_validate_coordinates_out_of_range_longitude(redis_token,
                                                           connection):
    token = await redis_token(**{'coordinates': [0, 181, 0]})
    position = Position(token, 1)
    position = await validate(connection, position)
    assert ('Longitude must be within [-180, 180]'
            in position.errors['coordinates'])


async def test_validate_coordinates_out_of_range_altitude(redis_token,
                                                          connection):
    token = await redis_token(**{'coordinates': [0, 0, 9001]})
    position = Position(token, 1)
    position = await validate(connection, position)
    assert "It's over 9000!!!" in position.errors['coordinates']


async def test_valid_payload_should_not_return_errors(redis_token,
                                                      mock_checks,
                                                      connection):
    token = await redis_token()
    received_at = datetime.now(timezone.utc).timestamp()
    position = Position(token, received_at)
    position = await validate(connection, position)
    assert position.errors == {}


async def test_check_conformity_populates_allowed(redis_token, mock_checks,
                                                  connection):
    token = await redis_token()
    position = Position(token, 1)
    position = await validate(connection, position)
    assert position.errors == {}
    assert position.payload['allowed'] is True
    assert position.payload['allowed_check_at'] is not None


async def test_check_conformity_return_false_on_forbidden_area(redis_token,
                                                               mock_checks,
                                                               connection):
    coordinates = [43.51, 5.35, 0.0]  # CTR AIX.
    mock_checks(allowed=False)
    token = await redis_token(**{'coordinates': coordinates})
    position = Position(token, 1)
    position = await validate(connection, position)
    assert position.errors == {}
    assert position.payload['allowed'] is False
    assert position.payload['allowed_check_at'] is not None


async def test_check_conformity_resilient_on_server_failure(redis_token,
                                                            monkeypatch,
                                                            connection):
    async def request(self, method, url, **kwargs):
        from aiohttp.client_exceptions import ClientConnectorError
        raise ClientConnectorError()

    monkeypatch.setattr('aiohttp.ClientSession._request', request)
    token = await redis_token(**{'coordinates': ALLOWED_POSITION})
    position = Position(token, 1)
    position = await validate(connection, position)
    assert position.errors == {}
    assert position.payload['allowed'] is None
    assert position.payload['allowed_check_at'] is None


async def test_check_registration_populates_registered(redis_token,
                                                       mock_checks,
                                                       connection):
    token = await redis_token()
    position = Position(token, 1)
    position = await validate(connection, position)
    assert position.errors == {}
    assert position.payload['registered'] is True
    assert position.payload['registered_check_at'] is not None


@pytest.mark.parametrize('registered', [True, False])
async def test_check_registration_is_cached(redis_token, mock_checks,
                                            connection, monkeypatch,
                                            registered):
    counter = 0
    mock_request = ClientSession._request

    async def request(self, method, url, *args, **kwargs):
        if '/account/device/' in url:
            nonlocal counter
            counter += 1
        return await mock_request(self, method, url, *args, **kwargs)

    monkeypatch.setattr(ClientSession, '_request', request)
    monkeypatch.setattr('wolf.config.REGISTRATION_CACHE_TTL', 1)
    mock_checks(registered=registered)
    # Regular call.
    token = await redis_token()
    position = Position(token, 1)
    position = await validate(connection, position)
    assert position.errors == {}
    assert position.payload['registered'] is registered
    assert position.payload['registered_check_at'] is not None
    # Cached call.
    position = await validate(connection, position)
    assert position.errors == {}
    assert position.payload['registered'] is registered
    assert position.payload['registered_check_at'] is not None
    assert counter == 1
    time.sleep(1)
    # Not anymore cached call.
    position = await validate(connection, position)
    assert position.errors == {}
    assert position.payload['registered'] is registered
    assert position.payload['registered_check_at'] is not None
    assert counter == 2


async def test_check_registration_is_not_cached(redis_token, mock_checks,
                                                connection, monkeypatch):
    counter = 0
    mock_request = ClientSession._request

    async def request(self, method, url, *args, **kwargs):
        if '/account/device/' in url:
            nonlocal counter
            counter += 1
            from aiohttp.client_exceptions import ClientConnectorError
            raise ClientConnectorError()

        return await mock_request(self, method, url, *args, **kwargs)

    monkeypatch.setattr(ClientSession, '_request', request)
    monkeypatch.setattr('wolf.config.REGISTRATION_CACHE_TTL', 1)
    mock_checks(registered=None)
    # Regular call.
    token = await redis_token()
    position = Position(token, 1)
    position = await validate(connection, position)
    assert position.errors == {}
    assert position.payload['registered'] is None
    assert position.payload['registered_check_at'] is None
    # Not cached call.
    position = await validate(connection, position)
    assert position.errors == {}
    assert position.payload['registered'] is None
    assert position.payload['registered_check_at'] is None
    assert counter == 2


async def test_check_registration_return_false_on_unknown_id(redis_token,
                                                             mock_checks,
                                                             connection):
    mock_checks(registered=False)
    token = await redis_token()
    position = Position(token, 1)
    position = await validate(connection, position)
    assert position.errors == {}
    assert position.payload['registered'] is False
    assert position.payload['registered_check_at'] is not None


async def test_check_registration_resilient_on_server_failure(redis_token,
                                                              monkeypatch,
                                                              connection):
    async def request(self, method, url, **kwargs):
        from aiohttp.client_exceptions import ClientConnectorError
        raise ClientConnectorError()

    monkeypatch.setattr('aiohttp.ClientSession._request', request)
    token = await redis_token()
    position = Position(token, 1)
    position = await validate(connection, position)
    assert position.errors == {}
    assert position.payload['registered'] is None
    assert position.payload['registered_check_at'] is None


async def test_creating_report_for_existing_key_updates(redis, count,
                                                        fetchrow, connection):
    redis.zadd('queue', 123456789, '{')  # Invalid payload.
    await process_queue(connection)
    assert await count() == 0
    assert await count('reports') == 1
    error = await fetchrow('reports')
    assert error['time'].timestamp() == 123456789
    redis.zadd('queue', 123456790, '{')
    await process_queue(connection)
    assert await count('reports') == 1
    error = await fetchrow('reports')
    assert error['time'].timestamp() == 123456790
    assert (json.loads(error['errors'])['format'] ==
            "Unreadable signed payload b'{'")
    assert bytes(error['data']) == b'{'


async def test_does_not_fail_on_nul_terminated_strings(redis, count,
                                                       mock_checks,
                                                       connection,
                                                       register_device,
                                                       private_key):
    data = {
        'time': datetime.now(timezone.utc).isoformat(),
        'registration': 'tesuni123456789012fr',
        'coordinates': ALLOWED_POSITION
    }
    await register_device(data['registration'])
    data = jwt.encode(data, private_key, algorithm=ALGORITHM)
    redis.zadd('queue', 123456789, data + b'\n\x00')
    await process_queue(connection)
    assert await count() == 1


async def test_report_with_nul_terminated_string_does_not_fail(redis, count,
                                                               connection):
    redis.zadd('queue', 123456789,
               '{"invalid json",\n}\n\x00')
    await process_queue(connection)
    assert await count() == 0
    assert await count('reports') == 1


async def test_check_positions_conformity_command(pg_payload, fetchrow,
                                                  mock_checks):
    await pg_payload(allowed_check_at=None, allowed=None)
    row = await fetchrow()
    assert row['allowed_check_at'] is None
    assert row['allowed'] is None
    await check_positions_conformity()
    row = await fetchrow()
    assert row['allowed_check_at'] is not None
    assert row['allowed'] is True
